############################################################################
#                                                                          #
# Copyright (c) 2023 Combine Control Systems AB                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
# There's no point to reiterate the documentation for "description"
# and standard "prepare", "analysis" and "systhesis" don't need one either
# pylint: disable=missing-module-docstring,missing-function-docstring

from collections import Counter, defaultdict


# lower-case global constant, but I don't get to decide this
# pylint: disable=invalid-name
description = """
Method to do the group-by or split-apply-combine pattern across a dataset.
This method is equivalent to the sql and pandas group by, though with fewer
options for the apply function.

The data is split on the columns in the "split_columns" options and then the operation
in the apply_operations are done on the apply_columns. The original rows per split can
also be counted if the count option is true. The resulting table is stored as a dataset.

This method does not support a previous, and that is a design-decision.
previous is used for chaining, and group_by modifies a dataset shape and
columns. These two concepts interact poorly with each other, as we want
to have flexible delta sizes, and don't have results (e.g: accumulation on
columns) depend on them. Therefore, using previous in group_by is an
anti-pattern. If this method ever needs to be optimized, the alternative would
be to limit the time scope with some option, or to aggregate deltas with previous
runs.
"""
datasets = (["source"],)
options = {
    "split_columns": [str],  # List of columns to split the data on
    "count": True,  # Whether to create a new column counting the number of rows in the split_columns
    # List of operations to apply to the "apply_columns". Valid values are: "sum" (more to come!)
    # The list shall either be of the same length as the "apply_columns", or of length 1 if the same
    # operation should be applied to all the columns
    "apply_operations": [str],
    "apply_columns": [str],  # List of columns to sum
}


def prepare(job):
    l = len(options.apply_operations)
    if l > 1:
        assert l == len(
            options.apply_columns
        ), "The apply_operations shall either be of the same length as the apply_columns, or of length 1"
    all_op = list(set(options.apply_operations))
    assert len(all_op) == 0 or (
        len(all_op) == 1 and all_op[0] == "sum"
    ), "sum is the only allowed operation"

    # Fetch the columns from the first dataset, and make sure that all others
    # have equivalent columns
    columns = {}
    for c in options.split_columns + options.apply_columns:
        columns[c] = (
            datasets.source[0].columns[c].type,
            datasets.source[0].columns[c].none_support,
        )
    for ds in datasets.source:
        for name, (col_type, none_support) in columns.items():
            assert name in ds.columns, f"Column {name} missing in ds {ds}"
            assert (
                col_type == ds.columns[name].type
            ), f"Column {name} as non-matching type in ds {ds}"
            assert (
                none_support == ds.columns[name].none_support
            ), f"Column {name} as non-matching none_suppot in ds {ds}"
    if options.count:
        columns["count"] = "int64"

    # Fetch the hashlabel from source[0] if it's valid.
    # If other sources have a different one, they'll crash during iterate
    hashlabel = (
        datasets.source[0].hashlabel
        if datasets.source[0].hashlabel in options.split_columns
        else None
    )

    dw = job.datasetwriter(columns=columns, hashlabel=hashlabel)
    return dw


def write_data(writer, cdict):
    for k, data in cdict.items():
        row = defaultdict(lambda: None, zip(options.split_columns, k))
        row.update(dict(data))
        writer(row)


def analysis(sliceno, prepare_res):
    cdict = defaultdict(Counter)
    cols = options.split_columns + options.apply_columns
    split_l = len(options.split_columns)
    tot_l = len(cols)
    dw = prepare_res
    for d in datasets.source.iterate_chain(sliceno, cols, hashlabel=dw.hashlabel):
        key = tuple(d[:split_l])
        if options.count:
            cdict[key]["count"] += 1
        for i in range(split_l, tot_l):
            # The columns can have None values
            if d[i] is not None:
                cdict[key][cols[i]] += d[i]

    # If there's a hashlabel, we know that the original data has it in a column
    # whose values we aren't modifying. Therefore, it's safe to write in parallel
    if dw.hashlabel:
        writer = dw.write_dict
        write_data(writer, cdict)
        cdict = None  # Signal that we already wrote and consumed the data

    return cdict


def synthesis(analysis_res, prepare_res):
    cdict = analysis_res.merge_auto()
    if cdict is not None:  # We didn't consume the data yet, write it!
        dw = prepare_res
        writer = dw.get_split_write_dict()
        write_data(writer, cdict)
