############################################################################
#                                                                          #
# Copyright (c) 2023 Combine Control Systems AB                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
"""
These are a set of functions related to the urd. Every function placed here
should have 'urd' as its first argument.
"""

from accelerator.build import JobList


def user_truncate_check(urd, urd_key):
    """
    Truncate the said urd_key, if truncating is allowed for the user.
    Common cases where the user is not allowed to truncate is production users,
    whose urd history should never be erased. In those cases, this function does
    nothing.

    :param urd: The urd
    :type urd: :class:`accelerator.build.Urd`
    :param urd_key: The key prefix to which possibly truncate
    :type urd_key: str
    """
    if urd._user not in ("production", "staging"):  # pylint: disable=protected-access
        urd.truncate(urd_key, 0)


# pylint: disable=too-many-arguments,too-many-positional-arguments
# Ideally, data_source parameter would be before entity_id. This is because
# every urd session has some kind of data source, but not all have entities.
# Unfortunately, in projects we initially did not use named parameters, so we
# cannot change the order. If we ever fix the projects, we could change the order
def build_urd_key(
    urd, stage, entity_id=None, data_source=None, write=False, key_filter="latest"
):
    """
    Build an urd key with a predefined format for a certain set of common
    stage values. The function can be used both for reading and writing
    depending on the write parameter. When reading, if the key exists for
    the ``production`` user, and it's more recent than for our user, it will be used.
    If ``key_filter`` is provided, ``urd.get(key, key_filter)`` will be used to get
    the values for the `most-recent` comparison. This function greatly
    simplifies the logic in the build scripts to get data from production.

    To use this function, it is a requisite to have a ``urd_scripts_version.py``
    file at the root of the accelerator project. The said file must contain
    a ``STAGE_VERSION`` variable that can be imported and used as a string,
    to build the correct key.

    :param urd: The urd
    :type urd: :class:`accelerator.build.Urd`
    :param stage: The stage from which to build the key, e.g: 'import'
    :type stage: str
    :param entity_id: an id to append to the key, e.g: unit number
    :type entity_id: any object with a string representation
    :param data_source: a string to identify the data source from which the data is coming, e.g: database1, filetype2
    :type data_source: str
    :param write: whether to build the key for writing. Controls whether we
        should look for 'production' data.
    :type write: bool
    :param key_filter: a filter for urd.get call to get entries in the urd log.
        Mostly useful to get data from production from maximum a certain data for
        e.g: training a model weekly
    :type key_filter: str
    :raises ImportError: When ``urd_scripts_versions`` is missing
    :raises AttributeError: When ``STAGE_VERSION`` cannot be imported from
        ``urd_scripts_versions``.
    :return: the requested urd key
    :rtype: str
    """
    try:
        import urd_scripts_versions as v  # pylint: disable=import-outside-toplevel

        # After introducing "data_source", we could use '==' instead of
        # 'startswith'. But keep it as-is for backwards-compatibility
        if stage.startswith("mangle"):
            version = v.MANGLE_VERSION
        elif stage.startswith("import"):
            version = v.IMPORT_VERSION
        elif stage.startswith("preprocess"):
            version = v.PREPROCESS_VERSION
        elif stage.startswith("model"):
            version = v.MODEL_VERSION
        else:
            raise AssertionError(
                f"urd_key '{stage}' not supported: must be one of 'mangle', 'import', 'preprocess', or 'model'"
            )
    except ImportError as exc:
        raise ImportError(
            "Could not import 'urd_scripts_versions', the file is possibly missing from the accelerator project root!"
        ) from exc
    except AttributeError as exc:
        raise AttributeError(
            f"{stage.upper()}_VERSION is missing in urd_scripts_version"
        ) from exc

    user_key = f"{stage}-{version}"
    # Ideally in the future data_source would either have a default value, or be
    # required in order to have an entity_id. This is more logical, as every
    # urd session has a data_source, and we might want to make it explicit (or
    # else have a default value like "default"). However, for the shake of
    # backwards compatibility, and until we have experience with adding
    # data_source parameters everywhere, data_source and entity_id follow the
    # same pattern in code
    if data_source is not None:
        user_key += f"-{data_source}"
    if entity_id is not None:
        user_key += f"-{entity_id}"
    # We are only allowed to write to our user.
    # Reading is allowed from any user.
    if write:
        return user_key

    # We take production only if it's more recent than non-production.
    # The assumption is that if there is a non-production key which is at least
    # as recent as production, the user run it on purpose, and it should be
    # picked.
    prod_key = f"production/{user_key}"
    prod_ts = urd.peek(prod_key, key_filter).timestamp
    # Optimization to avoid a warning and calls to the urd
    if urd._user == "production":  # pylint: disable=protected-access
        assert prod_ts != "0", f"urd key '{prod_key}' does not exist!"
        return prod_key
    user_ts = urd.peek(user_key, key_filter).timestamp
    assert prod_ts != "0" or user_ts != "0", f"urd key '{user_key}' does not exist!"
    if user_ts >= prod_ts:
        urd.warn(f"fetching non-production urd key for '{user_key}'")
        return user_key
    return prod_key


def _fetch_one(urd, key, record, key_filter):
    session = None
    if record:
        session = urd.get(key, key_filter)
    else:
        session = urd.peek(key, key_filter)
    for job in reversed(session.joblist):
        if len(job.datasets) >= 1:
            ts = session.timestamp  # pylint: disable=invalid-name
            return job, ts
    raise AssertionError(f"No jobs with a dataset in session for '{key} {key_filter}'")


# pylint: disable=too-many-positional-arguments
def _fetch_many(urd, stage, entities, data_source, record, key_filter):
    joblist = JobList()
    timestamps = []
    for entity_id in entities:
        key = build_urd_key(urd, stage, entity_id, data_source, key_filter=key_filter)
        # pylint: disable=invalid-name
        job, ts = _fetch_one(urd, key, record, key_filter)
        joblist.extend([job])
        timestamps.append(ts)

    return joblist, timestamps


def get_for_entities(urd, stage, entity_func, date, data_source=None):
    """
    Calls `urd.get(key, <=date)` on every entity from `entities`, using
    :func:`build_urd_key` to build the keys, and returning the latest available
    job with a dataset for the corresponding session, and the maximum timestamp

    :param urd: The urd
    :type urd: :class:`accelerator.build.Urd`
    :param stage: The stage to pass to :func:`build_urd_key`
    :type stage: str
    :param entity_func: Function that can be called with the urd as single parameter and that returns an iterable over entities
    :type entity_func: func(urd) -> iterable(str)
    :param date: The latest date from which to fetch information
    :type date: anything accepted by urd.get()
    :param data_source: a string that identifies the data source from which the data is coming, e.g: database1, filetype2, used for `build_urd_key`
    :type data_source: str
    :returns: A tuple with the list of jobs as first value, and the list of timestamps as the second one.
    :rtype: tuple(:class:`accelerator.build.JobList`, list)
    """
    return _fetch_many(urd, stage, entity_func(urd), data_source, True, f"<={date}")


def latest_for_entities(urd, stage, entity_func, data_source=None):
    """
    Calls `urd.get(key, "latest")` on every entity from `entities`, using
    :func:`build_urd_key` to build the keys, and returning the latest available
    job with a dataset for the corresponding session and the maximum timestamp

    :param urd: The urd
    :type urd: :class:`accelerator.build.Urd`
    :param stage: The stage to pass to :func:`build_urd_key`
    :type stage: str
    :param entity_func: Function that can be called with the urd as single parameter and that returns an iterable over entities
    :type entity_func: func(urd) -> iterable(str)
    :param data_source: a string that identifies the data source from which the data is coming, e.g: database1, filetype2, used for `build_urd_key`
    :type data_source: str
    :returns: A tuple with the list of jobs as first value, and the list of timestamps as the second one.
    :rtype: tuple(:class:`accelerator.build.JobList`, list)
    """
    return _fetch_many(urd, stage, entity_func(urd), data_source, True, "latest")


def peek_latest_for_entities(urd, stage, entity_func, data_source=None):
    """
    Calls `urd.peek(key, "latest")` on every entity from `entities`, using
    :func:`build_urd_key` to build the keys, and returning the latest available
    job with a dataset for the corresponding session, and the maximum timestamp

    :param urd: The urd
    :type urd: :class:`accelerator.build.Urd`
    :param stage: The stage to pass to :func:`build_urd_key`
    :type stage: str
    :param entity_func: Function that can be called with the urd as single parameter and that returns an iterable over entities
    :type entity_func: func(urd) -> iterable(str)
    :param data_source: a string that identifies the data source from which the data is coming, e.g: database1, filetype2, used for `build_urd_key`
    :type data_source: str
    :returns: A tuple with the list of jobs as first value, and the list of timestamps as the second one.
    :rtype: tuple(:class:`accelerator.build.JobList`, list)
    """
    return _fetch_many(urd, stage, entity_func(urd), data_source, False, "latest")


# pylint: disable=too-many-arguments
def begin_preprocess(
    urd, entity_id=None, data_source=None, get_log_dates_func=None, update=False
):
    """
    Prepares the correct urd-keys for the preprocessing and links it to the
    import that was requested. When using this function, the preprocess script
    of the project will look as follows:

    def main(urd):
        for entity_id in get_entity_ids(urd):
            import_job, urd_key, date = begin_preprocess(urd, entity_id)
            urd.build() commands ...
            urd.finish(urd_key, date)

    :param urd: The urd
    :type urd: :class:`accelerator.build.Urd`
    :param entity_id: an id to append to the key to pass to func:`build_urd_key`
    :type entity_id: any object with a string representation
    :param data_source: a string that identifies the data source from which the data is coming, e.g: database1, filetype2, used to build the urd key
    :type data_source: str
    :param get_log_dates_func: A function that can get the date of the last log file, so that the urd_session can be subscribed to that date. This function can be placed in the project_urd_helpers within the project and does not need to be used. If it is not provided, the date will be taken from the last import_session
    :type get_log_dates_func: func(urd, entity_id) -> date
    :param update: this is directly forwarded to `urd.begin`. It is mostly useful for production, when small changes are done to some scripts, that don't change the functionality, but would still prevent the urd from accepting them with the same urd key. In those cases, `update=True` allows the urd to not fail, and record again the same key.
    :type update: bool
    :returns: A tuple with the import job fetched, the urd_key for the
        preprocess, and the date used
    :rtype: tuple(:class:`accelerator.job.Job`, str, str)
    """
    urd_key = build_urd_key(urd, "preprocess", entity_id, data_source, write=True)
    user_truncate_check(urd, urd_key)

    urd.begin(urd_key, update=update)
    import_session = urd.latest(build_urd_key(urd, "import", entity_id, data_source))

    # See if we can obtain the date per delta, if not, take the date of the last import session
    if get_log_dates_func is None:
        date = import_session.timestamp
    else:
        date = get_log_dates_func(urd, entity_id)
        assert date == import_session.timestamp, "New data available but not imported!"

    print(f"{entity_id}")
    import_job = import_session.joblist.get(-1)
    return import_job, urd_key, date
