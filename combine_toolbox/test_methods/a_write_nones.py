############################################################################
#                                                                          #
# Copyright (c) 2023 Combine Control Systems AB                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
# There's no point to add documentation here
# pylint: disable=missing-module-docstring,missing-function-docstring

datasets = ("source",)


def prepare(job):
    dw = job.datasetwriter(caption=str("nones"), parent=datasets.source)
    dw.add("nones", "int64", none_support=True)
    return dw


def analysis(sliceno, prepare_res):
    dw = prepare_res
    for _ in datasets.source.iterate_chain(sliceno):
        dw.write(None)
