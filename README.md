# Combine's accelerator toolbox

This repository contains code used by Combine together with the [accelerator](https://expertmakeraccelerator.org/). We aim to have here any development of methods and utilities that we use at Combine together with the accelerator. There might be some of those which could become highly related to the way we work with the accelerator at Combine. But we hope that most of them will end up landing in the original accelerator's codebase. Therefore, although this code is copyrightable, it keeps on purpose the same free-software license as the accelerator.

## Usage

Most of the time, users just want to make use of the utilities and methods available in this repository. In projects that are run in the computational servers, this toolbox should have been automatically installed. If that's not the case, contact your system administrator. In the situation where you would want to run this toolbox locally, it is recommended to install it with pip:

```sh
$ pip install git+https://gitlab.com/combine-control-systems/accelerator-toolbox.git
```

## Development

For development, the easiest process is to make use of `PYTHONPATH` environment variable together with local changes. First clone this repository, and apply whichever changes you have in mind. Then, to start the server run:

```sh
$ export PYTHONPATH=/path/to/accelerator-toolbox
$ ax server
```

If you have also modified some of the build scripts in this repository, you will also need to add the variable in the terminal that does "run":

```
$ export PYTHONPATH=/path/to/accelerator-toolbox
$ ax run tests
```

replacing `/path/to/accelerator-toolbox` with the path where the git repository was cloned. You should now be able to `import combine_toolbox` in your methods and test with the changes you have done.

Finally, once you're done and you don't want to keep using your development files, make sure to unset the variable in all the terminals where you set it:

```sh
$ unset PYTHONPATH
```

Or otherwise close the terminal and use another one to stop using your changes.
