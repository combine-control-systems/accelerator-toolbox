############################################################################
#                                                                          #
# Copyright (c) 2023 Combine Control Systems AB                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
# There's no point to reiterate the documentation for "description"
# and standard "prepare", "analysis" and "systhesis" don't need one either
# pylint: disable=missing-module-docstring,missing-function-docstring
from accelerator.dataset import NoDataset

# lower-case global constant, but I don't get to decide this
# pylint: disable=invalid-name
description = r"""
Takes a dataset, and creates a new one from a subset of the rows

The rows are selected based on the values from "column" that match "values"
based on "condition"

To also retain the non-matching rows, "keep_not_match_ds" can be set to 'True',
and a dataset named 'not_match' will also be created
"""

datasets = ("source",)
jobs = ("previous",)

options = {
    # The name of the column from 'source' from which to take the values
    "column": str,
    # The values to make the matching again. There might be restrictions
    # depending on the selected condition
    "values": [],
    # The condition to select or discard rows. Possible values are:
    # * "in"
    # * "not in"
    # * "<" : together with only one comparison value in options-values
    # * ">" : together with only one comparison value in options-values
    "condition": "in",
    # Whether to keep non-matching rows in a 'not_match' dataset
    "keep_not_match_ds": bool,
}

CONDITIONS_TO_FUNCS = {
    "in": lambda v: v in options["values"],
    "not in": lambda v: v not in options["values"],
    "<": lambda v: v < options["values"][0],
    ">": lambda v: v > options["values"][0],
}


def prepare(job):
    assert (
        options.column in datasets.source.columns
    ), f"column {options.column} does not exist in source dataset. Possible values are {list(datasets.source.columns)}"
    assert (
        options.condition in CONDITIONS_TO_FUNCS
    ), f"condition {options.condition} is not valid. Possible values are {list(CONDITIONS_TO_FUNCS)}"
    assert not (
        options.condition in ["<", ">"] and len(options["values"]) != 1
    ), f"Invalid values for condition {options.condition}. Only one value is possible, not: {options['values']}"

    columns = {}
    for name, data in datasets.source.columns.items():
        columns[name] = (data.type, data.none_support)

    dw = job.datasetwriter(
        columns=columns,
        hashlabel=datasets.source.hashlabel,
        previous=jobs.previous.dataset() if jobs.previous else NoDataset,
    )
    dw_not_match = None
    if options.keep_not_match_ds:
        dw_not_match = job.datasetwriter(
            name="not_match",
            columns=columns,
            hashlabel=datasets.source.hashlabel,
            previous=jobs.previous.dataset("not_match") if jobs.previous else NoDataset,
        )

    return dw, dw_not_match


def analysis(sliceno, prepare_res):
    dw, dw_not_match = prepare_res
    cols = datasets.source.columns.keys()
    func = CONDITIONS_TO_FUNCS[options.condition]
    for data in datasets.source.iterate_chain(
        sliceno,
        cols,
        stop_ds={jobs.previous.dataset() if jobs.previous else NoDataset: "source"},
    ):
        d = dict(zip(cols, data))
        if func(d[options.column]):
            dw.write_dict(d)
        elif dw_not_match:
            dw_not_match.write_dict(d)
