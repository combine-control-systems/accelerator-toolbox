############################################################################
#                                                                          #
# Copyright (c) 2023 Combine Control Systems AB                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
# There's no point to reiterate the documentation for "description"
# and standard "prepare", "analysis" and "systhesis" don't need one either
# pylint: disable=missing-module-docstring,missing-function-docstring
from itertools import zip_longest
from collections import defaultdict
import pandas as pd

# lower-case global constant, but I don't get to decide this
# pylint: disable=invalid-name
description = r"""
Accelerator Dataset to pandas DataFrame.

The accelerator dataset is transformed to a pandas dataframe, that is provided
as the result of the job. If a list of datasets is provided, they will all
be iterated sequentially.

Since all functions of pandas do not deal well with columns that contain a dict,
all json columns containing dicts are flattened out.
For example, if the dataset contains the column I with
{'ST1': 10, 'ST2':3, ...}, the dataframe will now contain the columns
I_ST1, I_ST2, etc.
If one wants to use another character than the '_' to join the names,
one can set this with the option json_flatten_char.

If filename exists, the dataframe will also be written as a csv to the results
folder, and the file registered as part of the job.

If index exists, it must reference a column name that exists in all datasets.
The resulting dataframe will be indexed according to DataFrame.set_index and the
column name.

If columns exists, the dataframe will only contain those columns that are provided
in that list. If it does not exist, all columns of the dataset will be included
in the dataframe.

If sort_columns exists, the dataframe will be sorted according to
'sort_values(by=options.sort_columns)'

If fill_in_data is set to False (default), all datasets and deltas should have
the same columns, otherwise an error is raised.
However, the accelerator can automatically deal with this missing data, which
pandas cannot. Therefore we need to fill the missing data in with 'None' if
we want to use these columns anyway. This is a choice that can be made by the
user by setting it to True.
"""

datasets = (["source"], ["previous"])
options = {
    "filename": str,  # name under which to save the file. It should not be a path
    "index": str,  # column to use as index
    "columns": [],  # list of columns that will be included in the dataframe
    # By default, these columns need to exist in all deltas of all datasets.
    # However, if fill_in_data is set to True (see below),
    # it can contain columns that are missing in some datasets.
    "sort_columns": [],  # list of columns to pass to DataFrame.sort_values(by)
    "fill_in_data": False,  # if True, allow incomplete datasets and fill with None
    "json_flatten_char": "_",  # Character to use when flattening JSON columns
}


def flatten_row(row, ds_types, prev_types):
    """
    This function flattens all json columns in the data 'row'.
    But also non json columns pass through this function, since the data
    itself is wrapt in a dict. However, when a column contains a json,
    the function calls itself till there is no dict nesting left.

    The type of the cols and new flattened cols is checked if it is
    already defined in ds_types, if not: it is checked if it is the same
    as the found type for that flattened col in the previous row of data,
    which is saved in prev_types.
    """
    out = {}

    def flatten(d, name=""):
        if isinstance(d, dict):
            for key in d:
                flatten(
                    d[key], options.json_flatten_char.join([name, key]) if name else key
                )
        else:
            assert (
                name != ""
            ), "The dict contains an empty key, so no name is available for the column"
            out[name] = d
            if name not in ds_types:
                prev_types[name] = type_flattened_col(d, prev_types[name], name)

    flatten(row)
    return out, prev_types


def type_flattened_col(data, prev_type, c):
    # JSON only supports 'number' = float in python
    # Unicode in ds = string in python
    type_dict = {
        int: "float64",
        float: "float64",
        str: "string",
        bool: "bool",
        type(None): "None",
    }

    try:
        ctype = type_dict[type(data)]
    except KeyError as e:
        raise RuntimeError(
            f"We have not specified how to handle a var of type {type(data)}"
        ) from e

    if prev_type == "None":
        return ctype
    if ctype in [prev_type, "None"]:
        # This means that it is the same as in the previous iteration,
        # so the data types are consistent.
        return prev_type
    raise TypeError(
        f"Column {c} contains several different types of data: {prev_type} and {ctype}"
    )


def update_ds_types(ds_types, cols, ds):
    ds2pandas = {"unicode": "string", "datetime": "datetime64[ns]"}
    for c in cols:
        if c in ds_types:
            continue
        dstype = ds.columns[c].type
        if dstype in ds2pandas:
            ds_types[c] = ds2pandas[dstype]
        else:
            ds_types[c] = dstype


def prepare():
    assert options.json_flatten_char, "Must not be empty!"
    common_cols = None
    all_cols = None
    ds_types = defaultdict(lambda: "None")
    for dataset, previous in zip_longest(datasets.source, datasets.previous):
        assert dataset, "previous dataset list is longer than source!"
        for ds in dataset.chain(stop_ds={previous: "source"} if previous else None):
            cols = set(ds.columns.keys())
            update_ds_types(ds_types, cols, ds)
            if all_cols is None:
                all_cols = set(cols)
                common_cols = set(cols)
                continue
            common_cols &= cols
            all_cols |= cols
            if not options.fill_in_data:
                assert (
                    all_cols == common_cols
                ), f"Not all datasets contain the same columns, the difference is: {all_cols.symmetric_difference(common_cols)} "

    if options.columns:
        for c in options.columns:
            assert (
                c in all_cols
            ), f"{c} column does not exist in any dataset: {all_cols}"
        all_cols = set(options.columns)

    if options.index:
        # Pandas can not deal with None values in the index, so the index
        # needs to exist in all datasets.
        assert (
            options.index in common_cols
        ), f"index '{options.index}' does not exist in all datasets: '{common_cols}'"
        all_cols.add(options.index)

    if options.sort_columns:
        # Pandas can deal with sorting with None values by placing them at the
        # beginning or end. But now chose to not allow that in this method,
        # since the sorting will be unreliable. I am not sure if this is a good choice.
        for c in options.sort_columns:
            assert (
                c in common_cols
            ), f"sort column '{c}' does not exist in all datasets: '{common_cols}'"
            all_cols.add(c)
    return list(all_cols), ds_types


def analysis(prepare_res, sliceno):
    df_rows = []
    all_cols, ds_types = prepare_res
    # prev_types will keep track of the types found while flattening json data.
    # we check if these created columns have the same type in every row of the data.
    prev_types = defaultdict(lambda: "None")

    for dataset, previous in zip_longest(datasets.source, datasets.previous):
        for ds in dataset.chain(stop_ds={previous: "source"} if previous else None):
            cols = list(set(all_cols) & set(ds.columns.keys()))
            for values in ds.iterate(sliceno, columns=cols):
                data = dict(zip(cols, values))
                # Unfold the json columns from the dataset
                data, prev_types = flatten_row(data, ds_types, prev_types)
                df_rows.append(data)
    # prev_types now contains all types from the flattened json cols.
    col_types = ds_types | prev_types

    return df_rows, col_types


def synthesis(job, analysis_res):
    df_rows, col_types = analysis_res.merge_auto()
    # Note that merge_auto() will take they key+value in df_type from
    # the last slice if they exist in multiple slices. There is no check
    # on whether these are the same across slices. We assume this to not
    # be a problem and that pandas will raise an error if the type is
    # incorrect.

    # Get only the types for the cols present in df_rows
    df_type = {}
    for c in df_rows[0].keys():
        if col_types[c] == "None":
            raise TypeError(
                f"Column {c} only contains None. We only support the export of columns to pandas that contain data."
            )
        df_type[c] = col_types[c]

    final_df = pd.DataFrame.from_dict(df_rows, orient="columns").astype(df_type)

    if options.index:
        final_df.set_index(options.index, inplace=True)
    if options.sort_columns:
        final_df.sort_values(by=options.sort_columns, inplace=True)
    if options.filename:
        final_df.to_csv(options.filename)
        job.register_file(options.filename)

    return final_df
