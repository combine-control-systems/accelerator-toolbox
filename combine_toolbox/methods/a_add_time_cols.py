############################################################################
#                                                                          #
# Copyright (c) 2023-2024 Combine Control Systems AB                       #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
# There's no point to reiterate the documentation for "description"
# and standard "prepare", "analysis" and "systhesis" don't need one either
# pylint: disable=missing-module-docstring,missing-function-docstring

# lower-case global constant, but I don't get to decide this
# pylint: disable=invalid-name
description = """
Add time-related columns to the dataset.

If 'date', 'time' or 'hour' are 'True', the source dataset is expected to have
a unicode column named 'timestamp' in the format '%Y%m%d%H%M%S'.

If 'dt_date' or 'dt_time are 'True', the source dataset is expected to have
a datetime column named 'datetime'.

For any other column, the source dataset is expected to have either a datetime
column named 'datetime' or a date column named 'dt_date'.
"""

datasets = ("source", "previous")
options = {
    "date": bool,  # Whether to write an unicode column named 'date' in format "%y%m%d"
    "dt_date": bool,  # Whether to write a datetime.date column named 'dt_date'
    "hour": bool,  # Whether to write an unicode column named 'hour' in format "%y%m%d%H"
    "time": bool,  # Whether to write an unicode column named 'time' in format "%H%M%S"
    "dt_time": bool,  # Whether to write a datetime.time column named 'dt_time'
    "week": bool,  # Absolute week number, considering year
    "week_nbr": bool,  # Week number within the year (0-53)
    # The year according to the week number. This could differ from "year"
    # For example, date d=2018-12-31 though on year 2018, is assigned by ISO to
    # the first week of 2019.
    "week_year": bool,
    "weekday": bool,  # Day of the week, Monday=1, Sunday=7
    "month": bool,  # Absolute month number, considering year
    "month_nbr": bool,  # Month number within the year (1-12)
    "year": bool,  # Year in 4-digit form
}


def prepare(job):
    dw = job.datasetwriter(
        parent=datasets.source,
        caption="with date column as str",
        previous=datasets.previous,
    )
    time_cols = []
    # (name, type, list of columns from least to most preferred)
    column_data = [
        ("date", "unicode", ["timestamp"]),
        ("dt_date", "date", ["datetime"]),
        ("hour", "unicode", ["timestamp"]),
        ("time", "unicode", ["timestamp"]),
        ("dt_time", "time", ["datetime"]),
        ("week", "int32", ["datetime", "dt_date"]),
        ("week_nbr", "int32", ["datetime", "dt_date"]),
        ("week_year", "int32", ["datetime", "dt_date"]),
        ("weekday", "int32", ["datetime", "dt_date"]),
        ("month", "int32", ["datetime", "dt_date"]),
        ("month_nbr", "int32", ["datetime", "dt_date"]),
        ("year", "int32", ["datetime", "dt_date"]),
    ]
    columns = set()
    for name, typ, column_options in column_data:
        if options[name]:
            dw.add(name, typ)
            selected_col = None
            for c in column_options:
                if c in columns:
                    selected_col = c
                    break
                # Last one gets priority!
                selected_col = c if c in datasets.source.columns else selected_col
            assert (
                selected_col is not None
            ), f"'{c}' is a requirement for '{name}': {datasets.source.columns.keys()}"
            columns.add(selected_col)
            time_cols.append((name, selected_col))

    return dw, columns, time_cols


# local functions are indeed local variables, but don't really add to the
# complexity, and it's easier to understand it placing them in the function
# than as global functions. So let's disable pylint
# pylint: disable=too-many-locals
def analysis(sliceno, prepare_res):
    dw, columns, time_cols = prepare_res

    names_to_funcs = {
        "date": lambda ts: ts[2:8],
        "dt_date": lambda dt: dt.date(),
        "hour": lambda ts: ts[:10],
        "time": lambda ts: ts[8:],
        "dt_time": lambda dt: dt.time(),
        "week": lambda d: d.isocalendar().year * 100 + d.isocalendar().week,
        "week_nbr": lambda d: d.isocalendar().week,
        "week_year": lambda d: d.isocalendar().year,
        "weekday": lambda d: d.isoweekday(),
        "month": lambda d: d.year * 100 + d.month,
        "month_nbr": lambda d: d.month,
        "year": lambda d: d.year,
    }

    for d in datasets.source.iterate_chain(
        sliceno, columns, stop_ds={datasets.previous: "source"}
    ):
        d = dict(zip(columns, d))
        data = {}
        for name, col in time_cols:
            data[name] = names_to_funcs[name](d[col])
        dw.write_dict(data)
