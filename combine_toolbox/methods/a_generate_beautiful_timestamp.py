############################################################################
#                                                                          #
# Copyright (c) 2023 Combine Control Systems AB                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
# There's no point to reiterate the documentation for "description"
# and standard "prepare", "analysis" and "systhesis" don't need one either
# pylint: disable=missing-module-docstring,missing-function-docstring

# lower-case global constant, but I don't get to decide this
# pylint: disable=invalid-name
description = """
Create a 'timestamp' column with our preferred format of '%Y%m%d%H%M%S'
using existent raw time information. This information can come either as date
and time columns, or as a timestamp column with an (usually) horrible format.
All input columns are expected to be in raw form, and will be decoded using
utf-8.

timestamp_column is mutually exclusive with date_column and time_column.

timestamp_format, if required, must be a list of 6 tuples, while date_format
and time_format must be a list of 3 tuples. Those tuples represent the indexes
from where to extract each of the values to construct our format, in order.
For example, the first tuple in date_format should be the indexes where to
extract the year from the date string.

If the tuples reference less than 2 characters, the string will be padded with
zeros to the left.
"""

datasets = ("source", "previous")
options = {
    "timestamp_column": str,  # The name of the timestamp column
    # Equivalent to %Y-%m-%d %H:%M:%S, with any separator
    "timestamp_format": [
        (0, 4),
        (5, 7),
        (8, 10),
        (11, 13),
        (14, 16),
        (17, 19),
    ],
    "date_column": str,  # The name of the date column
    "time_column": str,  # The name of the time column
    # Equivalent to %Y-%m-%d, with any separator
    "date_format": [
        (0, 4),
        (5, 7),
        (8, 10),
    ],
    # Equivalent to %H:%M:%S with any separator
    "time_format": [
        (0, 2),
        (3, 5),
        (6, 8),
    ],
}


def fmt_checks(fmt, name, tuples):
    assert (
        len(fmt) == tuples
    ), f"'{name}_format' must have length {tuples}, but has {len(fmt)}: {fmt}"
    for f in fmt:
        assert (
            len(f) == 2
        ), f"every element in '{name}_format' must have 2 values, but has {len(f)}"


def prepare(job):
    assert options.timestamp_column or (
        options.date_column and options.time_column
    ), "'timestamp column' is mutually exclusive with 'date_column' and 'time_column'"
    if options.timestamp_column:
        fmt_checks(options.timestamp_format, "timestamp", 6)
        assert (
            options.timestamp_column in datasets.source.columns
        ), f"'{options.timestamp_column}' is not a valid column for '{datasets.source}'"
        columns = [options.timestamp_column]
    else:
        fmt_checks(options.date_format, "date", 3)
        fmt_checks(options.time_format, "time", 3)
        assert (
            options.date_column in datasets.source.columns
        ), f"'{options.date_column}' is not a valid column for '{datasets.source}'"
        assert (
            options.time_column in datasets.source.columns
        ), f"'{options.time_column}' is not a valid column for '{datasets.source}'"
        columns = [options.date_column, options.time_column]
    dw = job.datasetwriter(
        caption="create timestamp from date+time",
        parent=datasets.source,
        previous=datasets.previous,
    )
    dw.add("timestamp", "unicode")
    return dw, columns


def parse_split_time_format(d, t):
    ts = ""
    for i, j in options.date_format:
        ts += d[i:j].zfill(2)
    for i, j in options.time_format:
        ts += t[i:j].zfill(2)
    return ts


def analysis(sliceno, prepare_res):
    dw, columns = prepare_res
    for data in datasets.source.iterate_chain(
        sliceno,
        columns,
        stop_ds={datasets.previous: "source"},
    ):
        if options.timestamp_column:
            data = data[0].decode("utf-8")
            ts = ""
            for i, j in options.timestamp_format:
                ts += data[i:j].zfill(2)
        else:
            d = data[0].decode("utf-8")
            t = data[1].decode("utf-8")
            ts = parse_split_time_format(d, t)
        dw.write(ts)
