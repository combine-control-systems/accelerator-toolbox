############################################################################
#                                                                          #
# Copyright (c) 2023 Combine Control Systems AB                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
# There's no point to add documentation here
# pylint: disable=missing-module-docstring,missing-function-docstring
# And there are lots of variables with short names
# pylint: disable=invalid-name

from datetime import datetime, timedelta, timezone
import hashlib


def prepare(job):
    dw = job.datasetwriter()
    dw.add("time val", "bytes")
    dw.add("location", "unicode")
    dw.add("value", "int64")
    return dw


def get_location(dt):
    """
    Get some pseudo-random number based on the dt, and limit
    the result to 3 values
    """
    res = int(dt.timestamp() / dt.day) % 3
    if res == 0:
        return "LND"
    if res == 1:
        return "GTB"
    if res == 2:
        return "LDK"
    return None


def analysis(sliceno, prepare_res):
    dw = prepare_res
    dt = datetime(2014, 1, 4, 0, 3, 46, tzinfo=timezone(timedelta()))
    for i in range(0, 1000):
        dt += timedelta(hours=1)
        ts = dt.strftime("%d/%m/%Y %H:%M:%S")
        ts = ts.encode("utf-8")
        location = get_location(dt)
        value = (
            int(dt.timestamp())
            + int(hashlib.sha256(location.encode("utf-8")).hexdigest(), 16) % 10**8
            + i % (sliceno + 1)
        ) % 1000
        dw.write_list([ts, location, value])
