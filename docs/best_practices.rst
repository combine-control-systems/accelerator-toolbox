Best practices
==============

Before reading this document, it is recommended to be acquainted with the
accelerator.

Coding for the accelerator
--------------------------

The accelerator is an intrinsically-parallel program which often requires a
slightly different coding approach than when developing code for other
situations. In addition to that, at Combine Control Systems we have some
additional conventions, for things that we regularly use.

Time-related variables
""""""""""""""""""""""

While working with the accelerator and user data, it is very common to deal
with time-series data. This data can be imported into the accelerator and used
in many different ways. To avoid the possible confusion associated with dealing
with these variables, we have establish a naming convention. The long-form
represents the name of the column, while the short-form in parenthesis
represents the most common abbreviation:

* ``date (d)``: The date as a string.
* ``dt_date (d)``: The date, as a ``datetime.date`` object
* ``time (t)``: The time of the day (hours, minutes, seconds) as a string
* ``dt_time (t)``: The time of the day, as a ``datetime.day`` object
* ``timestamp (ts)``: The combination of date and time as a string. Most often in
  the very short format: ``%Y%m%d%H%M%S``
* ``datetime (dt)``: A full ``datetime.datetime`` object
* ``epoch``: The epoch time

Optimization
---------------------

Iterators iterate is very fast

The urd
-------

The urd is the high-level control program from the accelerator. It's features
and purpose are well-described in the accelerator documentation. The
documentation here instead focuses on its uses, and the rationale under common
best practices. These are developed from experience in a variety of running
projects. However, before going into further details, let's discuss the basics
for which we use the urd:

* The urd is specially useful to provide traceability of method execution,
  their results, and their input data. It does not have to be on every commit,
  and shall not be used to replace the git history. It is instead complementary
  to it. Provided a certain urd key, and its date, the code used can be
  obtained, and the input data is also recorded. There does not need to be a
  direct mapping from commits to entries in the urd.
* It is also useful to understand what happened in previous runs, and the
  dependencies leading to results.
* Used correctly, it can allow to identify the full data and code paths that
  lead to deliver models and solutions to customers.

With these basics discussed, let's talk about the pipelines, with their role in
development, how we version the keys, and about more details regarding the urd
use at Combine.

The common pipeline
"""""""""""""""""""

Most projects follow a similar structure, with 4 distinct stages:

* preimport: This part of the pipeline takes care of preparing the data to be
  ingested into the accelerator. It is often the case that customers have data
  formats that are badly fitted for computer analysis. The preimport phase takes
  such data, and transforms it into something that can be fastly and effectively
  parsed. It provides the ability to test and iterate on new formats and
  processes that can be used to influence the client development.
* import: The data, either directly from the client or through the preimport, is
  ingested into the accelerator. This often consists of a combination of
  ``csvimport``, ``dataset_type``, and ``dataset_sort`` methods, that create a dataset
  or dataset chain that represent the input data
* preprocess: The raw data gets transformed, mostly by the addition of new
  columns to the dataset. The new columns are derived data from the original
  ones, but that are often needed in the later steps of the pipeline.
  Having them available early-on greatly simplifies the later tasks. Especially
  for state-variables, which are variables that depend on a value in a previous
  or later row. Instead of needing to loop through the data, one can just fetch
  the required columns if these are already created during the preprocess.
* model and analysis: they take the preprocess as input and either generate a
  model out of it, or produce graphs to visualize and understand the data.

urd keys and versioning
"""""""""""""""""""""""

The urd makes use of two identifiers for tracking code and data. The *urd-key*
(which can be any string), and a *timestamp* in the form of a string, an integer
or some date or datetime objects. The timestamp is used to provide what we could
consider as some *versioning* of the urd key. It is recommended to use it to
track data changes within an urd key. The urd key is itself the central point of
the tracking system. The accelerator provides no requirements for the urd keys.
However, we strongly recommend to establish some conventions to be used across
projects. Our own convention is implemented in :ref:`Combine's toolbox`, and
is based on the following rules:

* The urd key is split into different parts, separated by ``-`` characters. It
  is recommended to treat ``-`` as a reserved character.
* The first part is related to one of the pipeline stages mentioned above,
  e.g: *import*, or *preprocess*.
* The second part contains a high-level versioning for the pipeline stage.
  The urd already provides some versioning through timestamps. However, we
  recommend timestamps to only be used for changes in the data. When big
  refactorings or changes happen to the code related to part of the pipeline
  stages, there is the need to keep track of those changes. For example, to
  avoid doing analysis on results mixed from before and after the refactoring.
  When using the :ref:`Combine's toolbox`, that is done automatically
  by requesting the existence of a ``urd_scripts_versions.py`` file, used
  by a set of supported pipeline stages, that are described there in more
  detail.
* Finally, if there is the need to provide more refined tracking of some
  entities within the project, a third part can be added, which references
  the said entity. More details about this use-case will be provided in the
  following section.

In addition to all the above, the urd has the concept of *users*. The user is
automatically assigned by the urd considering the ``URD_AUTH`` environment
variable, or if missing the current user. Each urd key is always assigned to one
user. Every user can only write to their own keys, but can read the keys from
all other users. To do it, it is as simple as prefixing the urd key in calls
to the urd with the user from which to fetch it, like ``user/key``.

We make use of this feature extensively, through
:func:`combine_toolbox.build_urd_key`. This function, when appropriate, will
return an urd key prefixed with ``production/`` or ``staging/``, which warranty
that production or staging data will be used if necessary.


Common usage
""""""""""""

In general, one can find two different kind of projects with respect to the urd.
Those projects where there are a few distinct entities that produce the data.
And those where there's either only one, or too many, that either cannot be
effectively tracked, or whose tracking brings no benefit.

* For the few-distinct-entities situation we establish the convention of
  tracking them individually. This means, instead of a single urd key for each
  of the common pipeline stages, there are as many as entities, e.g:
  ``import-1-X`` and ``import-1-Y`` in case of entities ``X`` and ``Y``. Each of those
  keys will contain a dataset (or a dataset chain) with all the data
  corresponding to the said entity. This allows to very easily analyze and
  study the entities independently, as it's expected to find different
  behaviors. However, it is often also very useful to fetch data from every
  possible entity, given the fact that entity urd-keys follow a clear pattern.
  For that purpose, there exists the ``*_for_entities`` group of functions
  described in detail in :ref:`urd-related functions`.
* For the situation where there is a single entity or many entities which are
  not clearly distinguishable among each other, the additional complexity
  becomes unnecessary. In this case, there is a single urd key for each of the
  pipeline stages, that contains all the data as a dataset or dataset chain.
  When relevant, the entity name might be added as a column to the dataset.


Details on tracibility
""""""""""""""""""""""
This section describes details on the reasoning behind our current choices on how
we use the urd to keep tracibility. This section is as reference for ourselves.
We need to test it on more projects to ensure it is water tight.

To keep track of all data and code, combine makes use of:
* The urd from the accelerator keeps track of everything that has been ran, including
  the results, code and data that was used. For more information, see Chapter 7 of the
  manual (https://expertmakeraccelerator.org/pdf/acc_manual.pdf) and the text above.
* CI, which checks all updates in the code and gives an overview of all changes it does
  to the data and the results. This is needed to keep track of all the small changes
  that should not actually change any functionality.

2 other concepts that play an important role in how the CI and urd are set up are:
* Entity_id: the id of the unit/vessel/machine that collected the data
* Deltas: the new data that we receive comes in a file that only contains this new data,
  not the old data. These files we call deltas.

A few choices that have been made to use the urd in our projects:

* As mentioned above, we manually keep track of versions of the code, but only regarding
  big logical changes. This means that the urd database has a separate .urd file for every
  version of the code. (And also a separate one for every entity (unit/vessel/machine
  that collects data).)
* Every time a small change to the code gets made, this will be reflected by a new line
  in the transaction log inside the .urd file. This is possible by using the option
  update=True in the urd.begin(), which allows a session with the same urd_key and date to
  be saved. Since the .urd files can only be appended to, the older sessions with the same
  urd_key and date are still present and this allows us to keep the history in case any
  unexpected errors occur.
* We work with deltas, meaning that all new data that we receive from customers is stored
  in a new file that only contains the new data. However, the size of these new pieces of
  data can vary with time, arrive unordered, or be chaotic in general. Therefore,
  we made the urd logic not be dependent on the deltas.
  Instead, the mangle, import, and preprocess will take whatever new data is
  available to them, and create a new session while executing on it. This
  execution will obtain its own line in the transaction log within the .urd file.
  However, this has a second consequence. If no new data is received, and the
  user does not truncate (like in production), then we don't re-play the
  history. Instead, we just do nothing. For small code-changes that don't have
  an impact in the results, this is no issue. And the CI should let us know if
  anything actually changed. When big changes happen that affect the results, it
  is necessary to bump the versions in ``urd_scripts_versions.py``.
* We use CI to create diffs of the data and the model to be able to check if no
  functional changes have occurred. In order to allow the CI to run with the
  updated changes for all data, we use urd.truncate(0), which clears up all
  history (still visible in the log) and re-runs everything.
* To be able to free up memory and test scalability, the user workdirs need to be emptied
  from time to time. To not loose tracibility on models we have provided to the customers,
  we have a separate workdir (persistent) for production data, that is the only
  one available when doing production runs (runs of pipelines in main and dev).


How to deep-dive into the urd details
"""""""""""""""""""""""""""""""""""""

To record a run, the code contains an ``urd.begin(urd_key, date)`` and ends with
``urd.finish(urd_key)``. This results in a new file (urd_key.urd) in the urd
database, which you can find at ``/var/local/lib/ax/urd.db/<user-name>``.
Editing these files manually can have dire consequences. For safety, only read
them with ``tail <filename>``.
Each directory contains multiple ``urd_key.urd`` files with a transaction log
where every line represents a unique session. The details of the format are out
of the scope of this document, since the format is versioned and changes with
time. But should be possible to decipher with some knowledge of the urd and
the source code in ``accelerator/urd.py`` file.

One can also obtain all this information on the dashboard, that should be
accessible in your browser at ``http://localhost:8520/`` when connected to the
server and having the accelerator server running (``ax server``).
