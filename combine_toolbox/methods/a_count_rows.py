############################################################################
#                                                                          #
# Copyright (c) 2023 Combine Control Systems AB                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
# There's no point to reiterate the documentation for "description"
# and standard "prepare", "analysis" and "systhesis" don't need one either
# pylint: disable=missing-module-docstring,missing-function-docstring

from accelerator import subjobs
import matplotlib.pyplot as plt


# lower-case global constant, but I don't get to decide this
# pylint: disable=invalid-name
description = """
Method to count rows or accumulate their values across a dataset. The centering
point of the method is the "columns" options, which are the columns over which
data is accumulated or counted. The order in which they are provided is relevant
for things like sorting. Considering that, this method can:

* Count the amount of times the combination of "columns" happen. This is
  controlled by the "count" option.
* Accumulate the values of other columns over the combinations of "columns".
  The columns to accumulate are provided by the "accumulator_columns" list.

As a result, this method produces a dataset and saves it as a CSV. For the
options columns=['datetime', 'key'], count=True,
accumulator_columns=['cost','benefit'], the resulting dataset will have this
structure:

datetime;key;count;cost;benefit

In order to the counting, this methods calls group_by, and its resulting
dataset can be accessed with the name "group_by"

For legacy reasons, this method also returns a dictionary, that should not be
used in new implementations.

It also supports quick plotting when only counting values from one column
in the dataset. This can for example be used to quickly get an overview of
the amount of rows per day, but is currently deprecated, and new methods should
not use it.
"""
datasets = (["source"],)
options = {
    "columns": [str],  # List of columns to count or accumulate over
    # If True, the results will be sorted according to column names.
    # If False, the results will be sorted first based on the count value
    # or the accumulator columns, depending on the rest of the options,
    # and then based on the column names
    "sort_columns": False,
    "count": True,  # Whether to count the nested rows
    "accumulator_columns": [str],  # List of columns to count additionally
    "plot": bool,  # DEPRECATED: Whether to automatically plot the data
}


def prepare():
    if options.plot:
        l = len(options.columns)
        assert l == 1 and (
            len(options.accumulator_columns) > 0 or options.count
        ), "Plotting for quick analysis is only supported for one column and counting or accumulation"
    for c in options.accumulator_columns:
        assert datasets.source[0].columns[c].type in (
            "float32",
            "float64",
            "int32",
            "int64",
            "number",
        ), "Accumulator columns must be numeric"

    assert (
        options.count or len(options.accumulator_columns) >= 1
    ), "Count is False, but no accumulator_columns provided"


def synthesis(job):

    grouped_ds = subjobs.build(
        "group_by",
        source=datasets.source,
        split_columns=options.columns,
        count=options.count,
        apply_operations=["sum"],
        apply_columns=options.accumulator_columns,
    ).dataset()
    grouped_ds.link_to_here(name="group_by")

    if options.sort_columns:
        sort_columns = options.columns
    elif options.count:
        sort_columns = ["count"] + options.columns
    else:
        sort_columns = options.accumulator_columns + options.columns
    sorted_ds = subjobs.build(
        "dataset_sort",
        source=grouped_ds,
        sort_columns=sort_columns,
        sort_order="ascending" if options.sort_columns else "descending",
        sort_across_slices=True,
    ).dataset()
    sorted_ds.link_to_here()

    fn = f"counted_{'_'.join(options.columns)}.csv"
    subjobs.build(
        "csvexport",
        filename=fn,
        separator=";",
        labels=(
            options.columns
            + (["count"] if options.count else [])
            + options.accumulator_columns
        ),
        source=sorted_ds,
    ).link_to_here(fn)

    if options.plot and len(options.columns) == 1:
        col1 = options.columns[0]
        if options.count:
            col2 = "count"
        elif len(options.accumulator_columns) > 0:
            col2 = options.accumulator_columns[0]
        else:
            print("no values to plot")
        dates, values = zip(*sorted(sorted_ds.iterate(None, [col1, col2])))
        plt.plot(dates, values)
        plt.savefig(f"plot_{col2}_{col1}.png")
        job.register_file(f"plot_{col2}_{col1}.png")
        plt.close()
