.. Combine's accelerator toolbox documentation master file, created by
   sphinx-quickstart on Thu Jul 20 14:28:29 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Combine's accelerator toolbox's documentation!
=========================================================

Combine's accelerator toolbox contains a set of methods, helper functions, and
utilities for the use of the `accelerator <https://expertmakeraccelerator.org/>`_
at Combine Control Systems. A sensible amount of the resources available in this
toolbox could become part of the upstream project at some future point in time.
Therefore, this repository mostly contains reusable code that is not yet ready
to be provided to the accelerator authors.

In addition, this documentation covers a set of best practices and common
use-cases for the usage of the accelerator at Combine Control Systems. These are
provided to be useful for the accelerator authors and their community, so that
their and our needs remain aligned.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   best_practices

   methods
   functions


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
