############################################################################
#                                                                          #
# Copyright (c) 2023 Combine Control Systems AB                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
"""
These are a set of functions related to accelerator jobs. Every function here
should have 'job' or 'jobs' as its first argument.
"""

from pathlib import Path


def link(job, path="", suffix="", pickles=False):
    """
    Links data from a job into a pre-defined structure under the *results*
    directory. If the file is a csv or a pickle, it will be stored under
    directories with those same names. Otherwise, it will be in the root of
    the *results* directory.

    :param job: The job from which to link data
    :type job: :class:`accelerator.job.Job`
    :param path: Additional directory to create and store data under
    :type path: str | :class:`pathlib.Path`
    :param suffix: Suffix to add to the filename on link. Commonly used to
        differentiate results from methods that generate files with standard names
    :type suffix: str
    :param pickles: Whether or not to link pickles
    :type pickles: bool
    """
    for file in job.files():
        file = Path(file)
        file_name = file.with_stem(file.stem + suffix)
        if "pickle" in file.name:
            if pickles:
                job.link_result(file, linkname=Path("pickles", path, file_name))
        elif "csv" in file.name:
            job.link_result(file, linkname=Path("csv", path, file_name))
        else:
            job.link_result(file, linkname=Path(path, file_name))
