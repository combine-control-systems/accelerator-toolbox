############################################################################
#                                                                          #
# Copyright (c) 2023 Combine Control Systems AB                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
# There's no point to add documentation here
# pylint: disable=missing-module-docstring,missing-function-docstring
# And there are lots of variables with short names
# pylint: disable=invalid-name

CONST_VAL = 34


def main(urd):
    urd.begin("build_tests", 1)

    print("Generate datasets to test against")
    job_write_ds = urd.build("test_write_ds")

    job_const = urd.build(
        "add_const_columns",
        source=job_write_ds,
        columns={"id": ("unicode", str(CONST_VAL)), "num": ("int64", CONST_VAL)},
    )
    for i, n in job_const.dataset().iterate(None, ["id", "num"]):
        assert i == str(CONST_VAL), f"expected {CONST_VAL}, got {i}"
        assert n == CONST_VAL, f"expected {CONST_VAL}, got {n}"

    dict2column_tests(urd, job_const)
    time_methods_tests(urd, job_const)
    count_rows_test(urd, job_const)
    group_by_tests(urd, job_const)
    select_rows_tests(urd, job_write_ds)
    pivot_tests(urd, job_write_ds)
    pandas_tests(urd, job_const)

    urd.abort()


def dict2column_tests(urd, job_const):
    print()
    print("Testing methods related to dict2column")
    dict_const = {f"{CONST_VAL}": 10}
    job_num_10 = urd.build(
        "dict2column",
        source=job_const,
        key_column="id",
        dict=dict_const,
        column={"new_col": "int64"},
    )
    for i in job_num_10.dataset().iterate(None, "new_col"):
        assert i == 10
    # The values from "id" don't exist in dict_const, so None's should be added
    job_none_support = urd.build(
        "dict2column",
        source=job_const,
        key_column="num",
        dict=dict_const,
        column={"new_col": ("unicode", True)},
    )
    for i in job_none_support.dataset().iterate(None, "new_col"):
        assert i is None


def time_methods_tests(urd, job_const):
    print()
    print("Testing methods related to time columns")
    job_ts = urd.build(
        "generate_beautiful_timestamp",
        source=job_const,
        timestamp_column="time val",
        timestamp_format=[
            (6, 10),
            (3, 5),
            (0, 2),
            (11, 13),
            (14, 16),
            (17, 19),
        ],
    )
    # We assume that if typing goes right, "generate_build_timestamp" worked
    urd.build(
        "dataset_type",
        source=job_ts,
        rename={
            "timestamp": "datetime",
        },
        column2type={
            "datetime": "datetime:%Y%m%d%H%M%S",
        },
    )
    job_time_cols = urd.build(
        "add_time_cols",
        source=urd.joblist[-1],
        date=True,
        dt_date=True,
        hour=True,
        time=True,
        dt_time=True,
        week=True,
        week_nbr=True,
        week_year=True,
        weekday=True,
        month=True,
        month_nbr=True,
        year=True,
    )
    for wd in job_time_cols.dataset().iterate(None, "weekday"):
        assert 1 <= wd <= 7, wd

    # Also test that "add_time_cols" works with a "dt_date" column
    urd.build(
        "dataset_type",
        source=job_ts,
        rename={
            "timestamp": "dt_date",
        },
        column2type={
            "dt_date": "datei:%Y%m%d",
        },
    )
    urd.build(
        "add_time_cols",
        source=urd.joblist[-1],
        date=True,
        hour=True,
        time=True,
        week=True,
        week_nbr=True,
        week_year=True,
        weekday=True,
        month=True,
        month_nbr=True,
        year=True,
    )


def count_rows_test(urd, job_const):
    print()
    print("Testing count_row method and variations")
    count_order_data = """location;value;count
GTB;913;119
LND;90;113
GTB;513;112
LND;890;110
GTB;113;110
LND;690;106
LND;290;103
GTB;313;100
GTB;713;99
LND;490;96
LDK;469;94
LDK;269;94
LDK;869;88
LDK;669;78
LDK;69;78
GTB;714;41
LND;891;40
LND;691;40
GTB;514;36
GTB;114;36
LND;291;35
GTB;914;35
LDK;270;34
LND;91;33
GTB;314;32
LND;491;30
LDK;70;30
LDK;470;28
LDK;870;26
LDK;670;24
"""

    job_count = urd.build("count_rows", source=job_const, columns=["location", "value"])
    with job_count.open("counted_location_value.csv", encoding="utf-8") as f:
        data = f.read()
        assert data == count_order_data, f"expected '{count_order_data}', got '{data}'"
    job_count = urd.build(
        "count_rows",
        source=job_const,
        columns=["location", "value"],
        sort_columns=True,
    )
    with job_count.open("counted_location_value.csv", encoding="utf-8") as f:
        # We want to resort on column names
        def sort_order_data(k):
            k = k.split(";")
            return (k[0], int(k[1]))

        data = f.read()
        split = count_order_data.strip().split("\n")
        split = [split[0]] + list(sorted(split[1:], key=sort_order_data))
        lines = "\n".join(split)
        assert data.strip() == lines, f"expected '{lines}', got '{data}'"
    job_count = urd.build(
        "count_rows",
        source=job_const,
        columns=["location", "value"],
        count=False,
        accumulator_columns=["num"],
    )
    with job_count.open("counted_location_value.csv", encoding="utf-8") as f:
        data = f.read()
        # Get the original data and multiply it by "num" const value!
        lines = []
        for line in count_order_data.split("\n"):
            if not line:
                continue
            split = line.split(";")
            val = split[-1]
            if val == "count":
                val = "num"
            else:
                val = str(CONST_VAL * int(val))
            split[-1] = val
            lines.append(";".join(split))
        lines = "\n".join(lines)
        assert data.strip() == lines, f"expected '{lines}', got '{data}'"


def group_by_tests(urd, job_const):
    print()
    print("Testing group_by method and variations")
    # This method is used in count_row, so here we mostly test other options
    job_group_by = urd.build(
        "group_by",
        source=job_const,
        split_columns=["location", "value"],
        apply_operations=["sum"],
        apply_columns=["num"],
    )
    assert job_group_by.dataset().hashlabel is None

    # Check that writing on synthesis and analysis give the same results
    job_rehash_loc = urd.build(
        "dataset_hashpart", source=job_const, hashlabel="location"
    )
    job_group_by_hashed = urd.build(
        "group_by",
        source=job_rehash_loc,
        split_columns=["location", "value"],
        apply_operations=["sum"],
        apply_columns=["num"],
    )
    assert job_group_by_hashed.dataset().hashlabel == "location"
    gb = {}
    gbh = {}
    for l, v, n in job_group_by.dataset().iterate(None, ["location", "value", "num"]):
        gb[(l, v)] = n
    for l, v, n in job_group_by_hashed.dataset().iterate(
        None, ["location", "value", "num"]
    ):
        gbh[(l, v)] = n
    assert gb == gbh

    # Test None values
    job_nones = urd.build("write_nones", source=job_const)
    job_group_by_nones = urd.build(
        "group_by",
        source=job_nones,
        split_columns=["location", "value"],
        apply_operations=["sum", "sum"],
        apply_columns=["num", "nones"],
    )
    for num, none in job_group_by_nones.dataset().iterate(None, ["num", "nones"]):
        assert none is None
        assert num is not None


# pylint: disable=too-many-locals
def select_rows_tests(urd, job_write_ds):
    print()
    print("Testing select_rows method and variations")
    # We are going to select rows based on location. So first we're going to
    # count how many are there of each
    job_count = urd.build("count_rows", source=job_write_ds, columns=["location"])
    location_count = {}
    limit = 500
    up_sum = 0
    down_sum = 0
    exact_sum = 0
    for l, c in job_count.dataset().iterate(None, ["location", "count"]):
        location_count[l] = c
        if c < limit:
            down_sum += 1
        elif c > limit:
            up_sum += 1
        else:
            exact_sum += 1

    # Check different variations!
    job_select = urd.build(
        "select_rows", source=job_write_ds, column="location", values=["GTB"]
    )
    assert job_select.dataset().shape[1] == location_count["GTB"]
    job_select = urd.build(
        "select_rows",
        source=job_write_ds,
        column="location",
        values=["GTB", "LND", "BLAH"],
        keep_not_match_ds=True,
    )
    assert (
        job_select.dataset().shape[1] == location_count["GTB"] + location_count["LND"]
    )
    assert job_select.dataset("not_match").shape[1] == location_count["LDK"]
    job_select = urd.build(
        "select_rows",
        source=job_write_ds,
        column="location",
        values=["GTB"],
        condition="not in",
    )
    assert (
        job_select.dataset().shape[1] == location_count["LND"] + location_count["LDK"]
    )
    job_select = urd.build(
        "select_rows", source=job_count, column="count", condition="<", values=[limit]
    )
    res_select_down = job_select.dataset().shape[1]
    assert res_select_down == down_sum
    job_select = urd.build(
        "select_rows", source=job_count, column="count", condition=">", values=[limit]
    )
    res_select_up = job_select.dataset().shape[1]
    assert res_select_up == up_sum
    job_select = urd.build(
        "select_rows", source=job_count, column="count", condition="in", values=[limit]
    )
    res_select_exact = job_select.dataset().shape[1]
    assert res_select_exact == exact_sum
    assert res_select_down + res_select_up + res_select_exact == len(location_count)

    # Check that the hashlabel is propagated!
    job_rehash = urd.build(
        "dataset_hashpart", source=job_write_ds, hashlabel="location"
    )
    job_select = urd.build(
        "select_rows", source=job_rehash, column="location", values=["GTB"]
    )
    assert job_select.dataset().shape[1] == location_count["GTB"]
    assert job_select.dataset().hashlabel == "location"
    # We asked for GTB, on a hashlabel with "location". So all rows should be
    # in the same slice. This is easy to check by making sure the maximum and
    # the sum of the list of lines is identical!
    assert sum(job_select.dataset().lines) == max(job_select.dataset().lines)


def pivot_tests(urd, job_write_ds):
    print()
    print("Testing pivot method and variations")
    # We need a group_by because "time val" is duplicated in each slice
    job_group_by = urd.build(
        "group_by",
        source=job_write_ds,
        split_columns=["time val", "location"],
        apply_columns=["value"],
        apply_operations=["sum"],
        count=True,
    )
    job_pivot = urd.build(
        "pivot",
        source=urd.joblist[-1],
        columns=["time val"],
        label="location",
        value="value",
        hashlabel="time val",
    )
    # time val is unique per slice, so we should have half the rows
    pivot_shape = job_pivot.dataset().shape
    assert (
        pivot_shape[1] == job_write_ds.dataset().shape[1] / 2
    ), f"expected {job_write_ds.dataset().shape[1] / 2} rows, but got {pivot_shape[1]}"
    assert pivot_shape[0] == 4  # time val + 3 locations
    assert "GTB_value" in job_pivot.dataset().columns

    # hash dataset, check that hashlabel propagates!
    job_rehash = urd.build(
        "dataset_hashpart", source=job_group_by, hashlabel="location"
    )
    job_pivot = urd.build(
        "pivot",
        source=job_rehash,
        columns=["time val", "location"],
        label="value",
        value="count",
    )
    assert job_pivot.dataset().hashlabel == "location"


def pandas_tests(urd, job_const):
    # pylint: disable=unused-import,import-outside-toplevel
    try:
        import pandas as pd
    except ImportError:
        print("Skipping pandas tests, pandas not available!")
        urd.warn("Pandas tests skipped due to missing pandas!")
        return

    print()
    print("Testing pandas-related methods")
    urd.build(
        "add_const_columns",
        source=job_const,
        columns={"json_col": ("json", {"1": 54, "2": 37})},
    )
    urd.build(
        "dataset2dataframe", source=urd.joblist[-1], index="id", columns="json_col"
    )
    df = urd.joblist[-1].load()
    assert df.index.name == "id"
    assert df.index.values[0] == df.index.values[-1] == str(CONST_VAL)
    assert all(df.columns.values == ["json_col_1", "json_col_2"])
    # Check that the data in pandas matches what we introduced, in case
    # there is some bug in the translation of the column names
    assert all(df.iloc[0].values == [54, 37])
    assert all(df.iloc[-1].values == [54, 37])
