"""
The combine_toolbox.methods package is a package with a set of methods used
at Combine together with the accelerator. This package is not intended to be
imported manually, but instead to be added to the methods directive in
'accelerator.conf' as 'combine_toolbox.methods'. Most of this methods are
work in progress and we expect to see them included into the
'accelerator.standard_methods' at some point in the future.
"""
