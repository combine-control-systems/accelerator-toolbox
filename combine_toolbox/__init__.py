############################################################################
#                                                                          #
# Copyright (c) 2023 Combine Control Systems AB                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
"""
The combine_toolbox is a package to put together small functions that help
with recurrent needs related to the way we work at Combine. They have a bit
too many hard-coded values to be possibly upstreamed, although some of their
functionality could certainly be of use upstream. Here we import some of the
functions to make it easier to use the toolbox. One can simply do
'from combine_toolbox import link', and the user does not need to know about
the details of in which file does the 'link' function lives.
"""


from .job_helpers import link
from .urd_helpers import user_truncate_check
from .urd_helpers import build_urd_key
from .urd_helpers import get_for_entities
from .urd_helpers import latest_for_entities
from .urd_helpers import peek_latest_for_entities
from .urd_helpers import begin_preprocess
