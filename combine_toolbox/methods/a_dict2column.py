############################################################################
#                                                                          #
# Copyright (c) 2024 Combine Control Systems AB                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
# There's no point to reiterate the documentation for "description"
# and standard "prepare", "analysis" and "systhesis" don't need one either
# pylint: disable=missing-module-docstring,missing-function-docstring
from accelerator import JobWithFile

# lower-case global constant, but I don't get to decide this
# pylint: disable=invalid-name
description = r"""
Adds a new column from dict values, using an existing column as key.

This is a very common pattern, when we want to add some high-level information
from the values of a certain column.

The dict values can be passed in two ways:
* dict_from_job: a JobWithFile, where the build_script uses job.withfile() to
  pass the file. The job must have saved a dictionary with job.save()
* dict: a regular dictionary
"""

datasets = ("source", "previous")
options = {
    # A JobWithFile to load and use to create the new column from the values of
    # key_column
    "dict_from_job": JobWithFile,
    # A dictionary to use to create the new column from the values of key_column
    # Due to limitations in the accelerator, it currently only accepts strings
    # as keys
    "dict": {},
    "key_column": str,  # The name of the column whose data will be used to index the dict
    # The column to create based on the values from 'key_column' and the dict
    # column can be {'name': 'type'} or {'name': ('type', none_support)}
    "column": {},
}


def prepare(job):
    # The accelerator mangles the tuple in the options to construct a list!!
    columns = {}
    for k, v in options.column.items():
        columns[k] = v if not isinstance(v, list) else (v[0], v[1])
    dw = job.datasetwriter(
        parent=datasets.source,
        caption=f"with {columns}",
        previous=datasets.previous,
        columns=columns,
    )
    assert bool(options.dict) != bool(
        options.dict_from_job
    ), "One and only one of 'dict' or 'dict_from_job' must be passed"
    d = options.dict
    if not d:
        d = options.dict_from_job.load()
    return dw, d


def analysis(sliceno, prepare_res):
    dw, d = prepare_res
    for key in datasets.source.iterate_chain(sliceno, options.key_column):
        dw.write(d.get(key, None))
