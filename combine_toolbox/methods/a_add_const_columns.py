############################################################################
#                                                                          #
# Copyright (c) 2023 Combine Control Systems AB                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
# There's no point to reiterate the documentation for "description"
# and standard "prepare", "analysis" and "systhesis" don't need one either
# pylint: disable=missing-module-docstring,missing-function-docstring

# lower-case global constant, but I don't get to decide this
# pylint: disable=invalid-name
description = r"""
Add columns with a constant value.

The columns are a dictionary, with the name of the column as a key, and a tuple
as value containing first the column type, and then the actual value.
"""

datasets = ("source", "previous")

options = {
    # dict with columns to add. The keys must be strings with the column name,
    # and the values a tuple of the shape (column_type, column_value)
    "columns": {},
}


def prepare(job):
    assert (
        len(options.columns) > 0
    ), "columns argument is compulsory and must not be empty"
    dw = job.datasetwriter(
        caption=str(options.columns), parent=datasets.source, previous=datasets.previous
    )
    values = []
    for name, tup in options.columns.items():
        col_type = tup[0]
        col_value = tup[1]
        dw.add(name, col_type)
        values.append(col_value)
    return dw, values


def analysis(sliceno, prepare_res):
    dw, values = prepare_res
    for _ in datasets.source.iterate_chain(
        sliceno, stop_ds={datasets.previous: "source"}
    ):
        dw.write_list(values)
