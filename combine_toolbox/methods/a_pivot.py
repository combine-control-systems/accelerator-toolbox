############################################################################
#                                                                          #
# Copyright (c) 2024 Combine Control Systems AB                            #
#                                                                          #
# Licensed under the Apache License, Version 2.0 (the "License");          #
# you may not use this file except in compliance with the License.         #
# You may obtain a copy of the License at                                  #
#                                                                          #
#  http://www.apache.org/licenses/LICENSE-2.0                              #
#                                                                          #
# Unless required by applicable law or agreed to in writing, software      #
# distributed under the License is distributed on an "AS IS" BASIS,        #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. #
# See the License for the specific language governing permissions and      #
# limitations under the License.                                           #
#                                                                          #
############################################################################
# There's no point to reiterate the documentation for "description"
# and standard "prepare", "analysis" and "systhesis" don't need one either
# pylint: disable=missing-module-docstring,missing-function-docstring
from collections import defaultdict

datasets = ("source", "previous")
options = {
    "columns": [],  # The columns to keep intact
    "label": str,  # This column will set the label of the new columns
    "value": str,  # This column will populate the rows
    "hashlabel": str,  # Leave empty to inhering hashlabel from first source dataset if exists
}

# lower-case global constant, but we don't get to decide this
# pylint: disable=invalid-name
description = r"""
This method reshapes a dataset, turning it from a loggstyle (stacked) format to
a wide (record) format with fewer rows and more columns. Each unique value in
the label column will be transformed to a column name and the rows will be
populated by the value column.

A typical usecase is when the columns contain date-related data, the label
is a sensor name and the value a measurement of that sensor. Then the source
dataset will have three columns and the output dataset will have the columns
data and one column per sensor.

It is important that there is only one value for each combination for the
columns and labels, otherwise there will be a conflict. This is no problem if
the source dataset is created by group_by since it already have solved those
conflicts.

Beaware that this method does on-the-fly rehashing of the source dataset for
checking data conflicts. If the dataset is big, it might be more efficient to
hash the dataset before running this method.
"""


def prepare(job):
    value_type = datasets.source.columns[options.value].type
    new_columns = set()
    for label in datasets.source.iterate_chain(
        None, (options.label), stop_ds={datasets.previous: "source"}
    ):
        new_columns.add(label)
    new_columns = sorted(new_columns)

    dw = job.datasetwriter(
        previous=datasets.previous,
        hashlabel=options.hashlabel if options.hashlabel else datasets.source.hashlabel,
    )
    assert (
        dw.hashlabel in options.columns
    ), f"hashlabel '{dw.hashlabel}' must be part of options.columns"
    for column in options.columns:
        c = datasets.source.columns[column]
        dw.add(column, c.type, none_support=c.none_support)
    for col in new_columns:
        dw.add(f"{col}_{options.value}", value_type, none_support=True)
    return dw, new_columns


def analysis(sliceno, prepare_res):
    cdict = defaultdict(lambda: defaultdict(lambda: None))
    dw, new_columns = prepare_res
    for d in datasets.source.iterate_chain(
        sliceno,
        options.columns + [options.label, options.value],
        hashlabel=dw.hashlabel,
        rehash=True,
        stop_ds={datasets.previous: "source"},
    ):
        k = tuple(d[:-2])
        l = d[-2]
        assert (
            l not in cdict[k]
        ), f"There are several datapoints for the columns {k}, label {l}: {cdict[k]}"
        cdict[k][l] = d[-1]

    for cols, label2value in sorted(cdict.items()):
        data = list(cols)
        data.extend([label2value[label] for label in new_columns])
        dw.write_list(data)
